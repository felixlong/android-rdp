package com.softmedia.remote.widget;

import android.content.Context;
import android.inputmethodservice.Keyboard;

public class SpecialKeyboard extends Keyboard {
	public static final int SPECIAL_KEYCODE_SPECIALKEY 	= -99;
	public static final int SPECIAL_KEYCODE_SOFTINPUT 	= -98;
	public static final int SPECIAL_KEYCODE_POINTER		= -97;
	public static final int SPECIAL_KEYCODE_CTRL 		= -96;
	public static final int SPECIAL_KEYCODE_SHIFT 		= -95;
	public static final int SPECIAL_KEYCODE_ALT 		= -94;
	
    public SpecialKeyboard(Context context, int xmlLayoutResId) {
        super(context, xmlLayoutResId);
    }

    public SpecialKeyboard(Context context, int layoutTemplateResId, 
            CharSequence characters, int columns, int horizontalPadding) {
        super(context, layoutTemplateResId, characters, columns, horizontalPadding);
    }
}
