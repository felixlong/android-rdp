LOCAL_PATH := $(call my-dir)

#
## freerdp.a
#
include $(CLEAR_VARS)

libwinpr_src_files := 	            \
	winpr/libwinpr/asn1/asn1.c		\
	winpr/libwinpr/bcrypt/bcrypt.c	\
	winpr/libwinpr/credentials/credentials.c		\
	winpr/libwinpr/credui/credui.c	    \
	winpr/libwinpr/crt/alignment.c		\
	winpr/libwinpr/crt/buffer.c		    \
	winpr/libwinpr/crt/conversion.c		\
	winpr/libwinpr/crt/memory.c		    \
	winpr/libwinpr/crt/string.c		    \
	winpr/libwinpr/dsparse/dsparse.c    \
	winpr/libwinpr/environment/environment.c    \
	winpr/libwinpr/error/error.c        \
	winpr/libwinpr/file/file.c          \
	winpr/libwinpr/file/pattern.c       \
	winpr/libwinpr/handle/handle.c	    \
	winpr/libwinpr/handle/table.c	    \
	winpr/libwinpr/heap/heap.c		    \
	winpr/libwinpr/interlocked/interlocked.c		\
	winpr/libwinpr/io/io.c		        \
	winpr/libwinpr/library/library.c    \
	winpr/libwinpr/path/path.c          \
	winpr/libwinpr/pipe/pipe.c          \
	winpr/libwinpr/registry/registry.c	\
	winpr/libwinpr/registry/registry_reg.c	\
	winpr/libwinpr/rpc/rpc.c	        \
	winpr/libwinpr/rpc/ndr.c	        \
	winpr/libwinpr/rpc/ndr_array.c	    \
	winpr/libwinpr/rpc/ndr_context.c	\
	winpr/libwinpr/rpc/ndr_correlation.c\
	winpr/libwinpr/rpc/ndr_pointer.c	\
	winpr/libwinpr/rpc/ndr_private.c	\
	winpr/libwinpr/rpc/ndr_simple.c		\
	winpr/libwinpr/rpc/ndr_string.c	    \
	winpr/libwinpr/rpc/ndr_structure.c	\
	winpr/libwinpr/rpc/ndr_union.c	    \
	winpr/libwinpr/rpc/midl.c	        \
	winpr/libwinpr/sspi/NTLM/ntlm_av_pairs.c	\
	winpr/libwinpr/sspi/NTLM/ntlm_compute.c	    \
	winpr/libwinpr/sspi/NTLM/ntlm_message.c	    \
	winpr/libwinpr/sspi/NTLM/ntlm.c	            \
	winpr/libwinpr/sspi/Negotiate/negotiate.c	\
	winpr/libwinpr/sspi/Schannel/schannel.c	    \
	winpr/libwinpr/sspi/CredSSP/credssp.c	    \
	winpr/libwinpr/sspi/sspi.c	                \
	winpr/libwinpr/sspicli/sspicli.c	        \
	winpr/libwinpr/synch/address.c	            \
	winpr/libwinpr/synch/barrier.c	            \
	winpr/libwinpr/synch/condition.c	        \
	winpr/libwinpr/synch/critical.c	            \
	winpr/libwinpr/synch/event.c	            \
	winpr/libwinpr/synch/init.c	                \
	winpr/libwinpr/synch/mutex.c	            \
	winpr/libwinpr/synch/semaphore.c	        \
	winpr/libwinpr/synch/sleep.c	            \
	winpr/libwinpr/synch/srw.c	                \
	winpr/libwinpr/synch/synch.c	    \
	winpr/libwinpr/synch/timer.c	    \
	winpr/libwinpr/synch/wait.c	        \
	winpr/libwinpr/sysinfo/sysinfo.c	\
	winpr/libwinpr/thread/process.c	    \
	winpr/libwinpr/thread/processor.c	\
	winpr/libwinpr/thread/thread.c	    \
	winpr/libwinpr/thread/tls.c	        \
	winpr/libwinpr/timezone/timezone.c	\
	winpr/libwinpr/utils/cmdline.c	    \
	winpr/libwinpr/utils/ntlm.c	        \
	winpr/libwinpr/utils/print.c	    \
	winpr/libwinpr/utils/sam.c	        \
	winpr/libwinpr/utils/stream.c	    \
	winpr/libwinpr/winhttp/winhttp.c	\
	winpr/libwinpr/winsock/winsock.c	\

libfreerdp_cache_src_files := \
	libfreerdp/cache/brush.c	\
	libfreerdp/cache/pointer.c	\
	libfreerdp/cache/bitmap.c	\
	libfreerdp/cache/nine_grid.c	\
	libfreerdp/cache/offscreen.c	\
	libfreerdp/cache/palette.c	    \
	libfreerdp/cache/glyph.c		\
	libfreerdp/cache/cache.c		\
	
libfreerdp_codec_src_files := 		\
	libfreerdp/codec/bitmap.c			\
	libfreerdp/codec/color.c				\
	libfreerdp/codec/rfx_decode.c		\
	libfreerdp/codec/rfx_differential.c	\
	libfreerdp/codec/rfx_dwt.c			\
	libfreerdp/codec/rfx_encode.c		\
	libfreerdp/codec/rfx_pool.c			\
	libfreerdp/codec/rfx_quantization.c	\
	libfreerdp/codec/rfx_rlgr.c			\
	libfreerdp/codec/rfx.c				\
	libfreerdp/codec/nsc.c				\
	libfreerdp/codec/nsc_encode.c		\
	libfreerdp/codec/mppc_dec.c			\
	libfreerdp/codec/mppc_enc.c			\
	libfreerdp/codec/jpeg.c				\

libfreerdp_common_src_files := 		    \
	libfreerdp/common/addin.c			\
	libfreerdp/common/settings.c		\

libfreerdp_core_gateway_src_files := 	\
    libfreerdp/core/gateway/tsg.c                  \
	libfreerdp/core/gateway/rpc.c                  \
	libfreerdp/core/gateway/rpc_bind.c             \
	libfreerdp/core/gateway/rpc_client.c           \
	libfreerdp/core/gateway/rpc_fault.c            \
	libfreerdp/core/gateway/rts.c                  \
	libfreerdp/core/gateway/rts_signature.c        \
	libfreerdp/core/gateway/ntlm.c                 \
	libfreerdp/core/gateway/http.c                 \
	libfreerdp/core/gateway/ncacn_http.c           \

libfreerdp_core_src_files := 		\
    libfreerdp/core/activation.c                    \
	libfreerdp/core/extension.c                     \
	libfreerdp/core/gcc.c                           \
	libfreerdp/core/mcs.c                           \
	libfreerdp/core/nla.c                           \
	libfreerdp/core/nego.c                          \
	libfreerdp/core/info.c                          \
	libfreerdp/core/input.c                         \
	libfreerdp/core/license.c                       \
	libfreerdp/core/errinfo.c                       \
	libfreerdp/core/security.c                      \
	libfreerdp/core/settings.c                      \
	libfreerdp/core/orders.c                        \
	libfreerdp/core/freerdp.c                       \
	libfreerdp/core/graphics.c                      \
	libfreerdp/core/capabilities.c                  \
	libfreerdp/core/certificate.c                   \
	libfreerdp/core/connection.c                    \
	libfreerdp/core/redirection.c                   \
	libfreerdp/core/timezone.c                      \
	libfreerdp/core/rdp.c                           \
	libfreerdp/core/tcp.c                           \
	libfreerdp/core/tpdu.c                          \
	libfreerdp/core/tpkt.c                          \
	libfreerdp/core/fastpath.c                      \
	libfreerdp/core/surface.c                       \
	libfreerdp/core/transport.c                     \
	libfreerdp/core/update.c                        \
	libfreerdp/core/channel.c                       \
	libfreerdp/core/window.c                        \
	libfreerdp/core/listener.c                      \
	libfreerdp/core/peer.c                          \

libfreerdp_crypto_src_files := 		\
	libfreerdp/crypto/er.c			\
	libfreerdp/crypto/der.c			\
	libfreerdp/crypto/ber.c			\
	libfreerdp/crypto/per.c			\
	libfreerdp/crypto/certificate.c	\
	libfreerdp/crypto/crypto.c		\
	libfreerdp/crypto/tls.c			\
                            
libfreerdp_gdi_src_files := 	\
	libfreerdp/gdi/8bpp.c			\
	libfreerdp/gdi/16bpp.c			\
	libfreerdp/gdi/32bpp.c			\
	libfreerdp/gdi/bitmap.c		    \
	libfreerdp/gdi/brush.c			\
	libfreerdp/gdi/clipping.c		\
	libfreerdp/gdi/dc.c			    \
	libfreerdp/gdi/drawing.c		\
	libfreerdp/gdi/line.c			\
	libfreerdp/gdi/palette.c		\
	libfreerdp/gdi/pen.c			\
	libfreerdp/gdi/region.c		    \
	libfreerdp/gdi/shape.c			\
	libfreerdp/gdi/graphics.c		\
	libfreerdp/gdi/gdi.c            \
	                        
libfreerdp_locale_src_files := 		        \
	libfreerdp/locale/virtual_key_codes.c	\
	libfreerdp/locale/keyboard_layout.c	    \
	libfreerdp/locale/keyboard.c			\
	libfreerdp/locale/locale.c			    \
	libfreerdp/locale/timezone.c            \

libfreerdp_rail_src_files := 	    \
	libfreerdp/rail/window_list.c	\
	libfreerdp/rail/window.c		\
	libfreerdp/rail/icon.c			\
	libfreerdp/rail/rail.c			\
	                        
libfreerdp_utils_src_files :=       \
    libfreerdp/utils/dsp.c          \
    libfreerdp/utils/debug.c        \
	libfreerdp/utils/event.c        \
	libfreerdp/utils/bitmap.c       \
	libfreerdp/utils/hexdump.c      \
	libfreerdp/utils/list.c         \
	libfreerdp/utils/file.c         \
	libfreerdp/utils/passphrase.c   \
	libfreerdp/utils/pcap.c         \
	libfreerdp/utils/profiler.c     \
	libfreerdp/utils/rail.c         \
	libfreerdp/utils/signal.c       \
	libfreerdp/utils/sleep.c        \
	libfreerdp/utils/stopwatch.c    \
	libfreerdp/utils/stream.c       \
	libfreerdp/utils/string.c       \
	libfreerdp/utils/svc_plugin.c   \
	libfreerdp/utils/tcp.c          \
	libfreerdp/utils/thread.c       \
	libfreerdp/utils/time.c         \
	libfreerdp/utils/uds.c          \
	libfreerdp/utils/unicode.c      \

libfreerdp_channels_client_src_files := 	    \
	channels/client/channels.c	                \
	channels/client/tables.c                    \
	
libfreerdp_channels_cliprdr_client_src_files := \
	channels/cliprdr/client/cliprdr_format.c    \
	channels/cliprdr/client/cliprdr_main.c      \

libfreerdp_channels_drdynvc_client_src_files := \
	channels/drdynvc/client/drdynvc_main.c      \
	channels/drdynvc/client/dvcman.c            \
	channels/audin/client/audin_main.c          \
	channels/echo/client/echo_main.c            \
	channels/input/client/input_main.c            \
	channels/tsmf/client/tsmf_audio.c           \
	channels/tsmf/client/tsmf_codec.c           \
	channels/tsmf/client/tsmf_decoder.c         \
	channels/tsmf/client/tsmf_ifman.c           \
	channels/tsmf/client/tsmf_main.c            \
	channels/tsmf/client/tsmf_media.c           \
	channels/tsmf/client/ffmpeg/tsmf_ffmpeg.c   \

libfreerdp_channels_rail_client_src_files := 	\
	channels/rail/client/rail_main.c            \
	channels/rail/client/rail_orders.c          \
	
libfreerdp_channels_rdpdr_client_src_files := 	\
	channels/rdpdr/client/rdpdr_main.c          \
	channels/rdpdr/client/rdpdr_capabilities.c  \
	channels/rdpdr/client/irp.c                 \
	channels/rdpdr/client/devman.c              \
	channels/drive/client/drive_file.c          \
	channels/drive/client/drive_main.c          \
	channels/parallel/client/parallel_main.c    \
	channels/serial/client/serial_main.c        \
	channels/serial/client/serial_tty.c         \
	
libfreerdp_channels_rdpsnd_client_src_files := 	\
	channels/rdpsnd/client/rdpsnd_main.c        \
	channels/rdpsnd/client/audiotrack/audiotrack.cpp \
	channels/rdpsnd/client/audiotrack/rdpsnd_audiotrack.cpp \
	
libfreerdp_channels_sample_client_src_files := 	\
	channels/sample/client/sample_main.c        \

libfreerdp_client_common_src_files := 	\
    client/common/cmdline.c	\
	client/common/compatibility.c \
	client/common/client.c	\
	client/common/file.c	\

#LOCAL_ARM_MODE := arm
LOCAL_MODULE    := freerdp
LOCAL_CFLAGS := -DANDROID -DHAVE_CONFIG_H -DEXT_PATH=\"../bin/lib/freerdp/extensions\"

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
	libfreerdp_codec_src_files += libfreerdp/codec/rfx_neon.c.neon
	LOCAL_CFLAGS += -DWITH_NEON
endif

LOCAL_SRC_FILES := \
    $(libwinpr_src_files)           \
	$(libfreerdp_cache_src_files)   \
	$(libfreerdp_common_src_files)  \
	$(libfreerdp_codec_src_files)   \
	$(libfreerdp_core_src_files)    \
	$(libfreerdp_core_gateway_src_files)    \
	$(libfreerdp_crypto_src_files)  \
	$(libfreerdp_gdi_src_files)     \
	$(libfreerdp_locale_src_files)  \
	$(libfreerdp_rail_src_files)    \
	$(libfreerdp_utils_src_files)   \
	$(libfreerdp_channels_client_src_files) \
	$(libfreerdp_channels_cliprdr_client_src_files) \
	$(libfreerdp_channels_drdynvc_client_src_files) \
	$(libfreerdp_channels_rail_client_src_files) \
	$(libfreerdp_channels_rdpdr_client_src_files) \
    $(libfreerdp_channels_rdpsnd_client_src_files) \
	$(libfreerdp_channels_sample_client_src_files) \
	$(libfreerdp_client_common_src_files)   \

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)	\
	$(LOCAL_PATH)/include	\
	$(LOCAL_PATH)/include/freerdp	\
	$(LOCAL_PATH)/winpr/include	\
	$(LOCAL_PATH)/libfreerdp/core	\
	$(LOCAL_PATH)/libfreerdp/core/gateway	\
	$(LOCAL_PATH)/channels/tsmf/client \
	$(LOCAL_PATH)/channels/rdpsnd/client \
	$(LOCAL_PATH)/channels/rdpdr/client \
	$(LOCAL_PATH)/channels/drdynvc/client \
	$(LOCAL_PATH)/../openssl/include \
	$(LOCAL_PATH)/../jpeg \
	$(LOCAL_PATH)/../libffmpeg \

LOCAL_STATIC_LIBRARIES += cpufeatures

include $(LOCAL_PATH)/../android-ndk-profiler-3.1/android-ndk-profiler.mk
include $(BUILD_STATIC_LIBRARY)                                    
$(call import-module,android/cpufeatures)
